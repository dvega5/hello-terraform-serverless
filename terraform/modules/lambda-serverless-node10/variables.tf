variable "region" {
  type        = string
  description = "AWS region for deployment"
  default     = "eu-west-2"
}

variable "function_name" {
  type        = string
  description = "Function name"
  default     = "hello-world"
}

variable "s3_bucket" {
  type        = string
  description = "The S3 bucket that holds the packaged function"
  default     = "wh-terraform-hello"
}

variable "s3_key" {
  type        = string
  description = "The key location for the serverless package"
  default     = "v1.0.0/hello.zip"
}

variable "subdomain" {
  type        = string
  description = "Subdomain of the function URL"
  default     = "v0-1-1"
}

variable "handler" {
  type        = string
  description = "The handler for the serverless invocation"
  default     = "main.handler"
}

variable "runtime" {
  type        = string
  description = "The handler runtime for serverless invocation"
  default     = "nodejs10.x"
}

variable "domain" {
  type    = string
  default = "www.quantumstudios.dev"
}

variable "describe_function" {
  type        = string
  description = "A description of the function. Very inception."
  default     = "Describe me."
}

variable "edge_cert_arn" {
  type    = string
  default = "arn:aws:acm:us-east-1:280626511039:certificate/9f79a670-06ed-4a1b-a8ef-c30c6bd2a3f5"
}
