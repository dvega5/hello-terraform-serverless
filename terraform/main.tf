terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "eu-west-2"
}

module "lambda_serverless_latest" {
  source = "./modules/lambda-serverless-node10"

  function_name     = "hello-serverless-latest"
  describe_function = "Hello Serverless Terraform latest"

  subdomain = "latest"
  domain    = "go.willhallonline.net"

  s3_key    = "latest/hello.zip"
  s3_bucket = "hello-terraform-serverless"
}

module "lambda_serverless_v0_1_15" {
  source = "./modules/lambda-serverless-node10"

  function_name     = "hello-serverless-v0_1_15"
  describe_function = "Hello Serverless Terraform v0_1_15"

  subdomain = "v0-1-15"
  domain    = "www.quantumstudios.dev"

  s3_key    = "v0.1.15/hello.zip"
  s3_bucket = "hello-terraform-serverless"
}

module "lambda_serverless_v0_1_16" {
  source = "./modules/lambda-serverless-node10"

  function_name     = "hello-serverless-v0_1_16"
  describe_function = "Hello Serverless Terraform v0_1_16"

  subdomain = "v0-1-16"
  domain    = "go.willhallonline.net"

  s3_key    = "v0.1.16/hello.zip"
  s3_bucket = "hello-terraform-serverless"
}
